import json
from django.http import JsonResponse
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder, Conference
from django.views.decorators.http import require_http_methods

from .models import Attendee


class AttendeeListEncoder(ModelEncoder):
    """
    Encoder to use to list Attendees
    """

    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    """
    Encoder to present the details of an Attendee
    """

    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    GET request will utilize AttendeeListEncoder to get
    a list of Attendees for a specific Confernece

    POST request will create a new Attendee for a specific
    Conference, and use the AttendeeDetailEncoder. It will also
    catch the occurance of an invalid Conference being used.
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    """
    GET will take advantage of the AttendeeDetailEncoder
    to show the details of the requested Attendee. Catches
    error for invalid Attendee requested.

    DELETE will delete the specified Attendee. It will
    produce a TRUE / FALSE response to indicate if it
    successfully deleted any content

    PUT will update the specified Attendee. It will
    also catch the error if an invalid Attendee is
    specified. Utilizes the AttendeeDetailEncoder
    """
    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Attendee requested"},
                status=400,
            )

        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        try:
            Attendee.objects.filter(id=id).update(**content)
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee for update"},
                status=400,
            )

        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
