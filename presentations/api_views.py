import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from events.api_views import Conference

from .models import Presentation


class PresentationListEncoder(ModelEncoder):
    """
    Encoder to list Presentations
    """

    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    """
    Encoder for the Details of a Presentation
    """

    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    GET request will utilize PresentationListEncoder to get
    a list of Attendees for a specific Confernece. Catches
    error for invalid Presentation requested.

    POST request will create a new Presentation for a specific
    Conference, and use the PresentationDetailEncoder. It will also
    catch the occurance of an invalid Conference being used.
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    """
    GET will take advantage of the PresentationDetailEncoder
    to show the details of the requested Attendee

    DELETE will delete the specified Presentation. It will
    produce a TRUE / FALSE response to indicate if it
    successfully deleted any content

    PUT will update the specified Presentation. It will
    also catch the error if an invalid Presentation is
    specified. Utilizes the PresentationDetailEncoder
    """
    if request.method == "GET":
        try:
            presentation = Presentation.objects.get(id=id)
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation requested"},
                status=400,
            )

        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        try:
            Presentation.objects.filter(id=id).update(**content)
            presentation = Presentation.objects.get(id=id)
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation for update"},
                status=400,
            )

        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
