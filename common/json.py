from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    """
    Encoder to catch all Query Sets returned from a
    filter request on data
    """

    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    """
    Encoder to catch any datetime instances to be able
    to properly encode and pass thru JSON
    """

    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    """
    New encoder that takes advantage of the QuerySetEncoder,
    DateEncoder, and based on the JSONEncoder. This is now
    utilized to encode and present the RESTful API data to
    be sent via JsonResponse
    """

    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
