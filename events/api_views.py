import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

from .models import Conference, Location, State
from .acls import get_photo, get_weather_data


class LocationDetailEncoder(ModelEncoder):
    """
    Encoder to format the data for Location Details
    """

    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "photo_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    """
    Encoder to format the data for listing Locations
    """

    model = Location
    properties = ["name"]


class ConferenceListEncoder(ModelEncoder):
    """
    Encoder to format the list of Conferences
    """

    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    """
    Uses defined ModelEncoder to encode the details of a
    specified conference.
    """

    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

    def get_extra_data(self, o):
        return get_weather_data(o.location.city, o.location.state.abbreviation)


@require_http_methods(["POST", "GET"])
def api_list_conferences(request):
    """
    GET

    POST
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferneces": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods({"GET", "PUT", "DELETE"})
def api_show_conference(request, id):
    """
    GET will catch trying to get a Conference that does not exist
    I makes use of the ConferenceDetailEncoder to present the data

    DELETE will process the deletion of the requested Conference

    PUT will process an update to the specified Conference, or it
    will produce an error message for an invalid Location or
    Conference being specified
    """
    if request.method == "GET":
        try:
            conference = Conference.objects.get(id=id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference specified"},
                status=400,
            )

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location Specified"},
                status=400,
            )

        try:
            Conference.objects.filter(id=id).update(**content)
            conference = Conference.objects.get(id=id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location specified for update"},
                status=400,
            )

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    GET will use the LocationListEncoder to produce a list
    of locations

    POST will allow the creation of a new Location, and catch
    if an invalid state abbreviation is input. Makes use of the
    LocationDetailEncoder
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo_url = get_photo(content["city"], content["state"])
        content.update(photo_url)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    GET will utilize LocationDetailEncoder to show the details
    of the specified location, or catch an attempt to show the
    details of an invalid Location

    DELETE allows the deletion of the specified Location

    PUT makes use of the LocationDetailEncoder and allows for the
    update of a Location. Catches errors for an invalid state
    abbreviation and an invalid location being submitted
    """
    if request.method == "GET":
        try:
            location = Location.objects.get(id=id)
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location specified"},
                status=400,
            )

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        try:
            Location.objects.filter(id=id).update(**content)
            location = Location.objects.get(id=id)
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location specified for update"},
                status=400,
            )

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
